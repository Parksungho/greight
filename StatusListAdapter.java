package com.example.diarybaby;

import java.net.URL;
import java.util.Date;
import java.util.List;

import twitter4j.Status;
import twitter4j.User;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
/**
 * 트위터 연동 객체를 전달받기 위한 어뎁터 클래스
 * @author 박성호
 *
 */
public class StatusListAdapter extends BaseAdapter 
{
	public static String TAG = "StatusListAdapter";
	private Context mContext;
	private List<Status> mStatuses = null;
	Handler mHandler;
	/**
	 * 생성자
	 * @param Context context
	 * @param Handler handler
	 */
	public StatusListAdapter(Context context, Handler handler) 
	{
		mContext = context;
		mHandler = handler;
	}
	/**
	 * 객체 저장소 설정
	 * @param List<Status> list
	 */
	public void setListItems(List<Status> list)
	{
		mStatuses = list;
	}
	/**
	 * 저장소에 저장된 갯수 반환
	 * @return int
	 */
	public int getCount() 
	{
		if (mStatuses == null)
		{
			return 0;
		} 
		else
		{
			return mStatuses.size();
		}
	}
	/**
	 * 해당위치의 객체를 꺼내는 메소드
	 * @param int position
	 * @return Object
	 */
	public Object getItem(int position) 
	{
		if (mStatuses == null)
		{
			return null;
		} 
		else
		{
			return mStatuses.get(position);
		}
	}
	/**
	 * 모든 아이템이 선택가능한지 여부를 묻는 메소드
	 * @return boolean
	 */
	public boolean areAllItemsSelectable()
	{
		return false;
	}
	/**
	 * 해당위치의 객체가 선택 가능한지 묻는 메소드
	 * @param int position
	 * @return boolean
	 */
	public boolean isSelectable(int position) 
	{
		return true;
	}
	/**
	 * 객체를 얻는 메소드
	 * @param int position
	 * @return long
	 */
	public long getItemId(int position)
	{
		return position;
	}
	/**
	 * 해당 위치의 뷰를 얻기 위한 메소드
	 * @param int position
	 * @param View converView
	 * @param ViewGroup parent
	 */
	public View getView(int position, View convertView, ViewGroup parent)
	{
		StatusItemView itemView;
		if (convertView == null)
		{
			itemView = new StatusItemView(mContext);
		} 
		else
		{
			itemView = (StatusItemView) convertView;
		}

		try { //Status 에다가 해당 위치의 정보를 받아와 세팅해 준다.
			Status curStatus = mStatuses.get(position);
			User user = curStatus.getUser();
			String userName = user.getName();
			String userScreenName = user.getScreenName();
			URL url = new URL(user.getProfileImageURL());
			Date date = curStatus.getCreatedAt();
			String data = curStatus.getText();
			itemView.setText(0, userScreenName);
			String dateStr = TwitInfo.DateFormat.format(date);
			itemView.setText(1, dateStr);
			itemView.setText(2, data);
			if (url != null) 
			{
				Log.d(TAG, "Bitmap URL : " + url);
				StatusItemView itemView1=itemView;
				URL url1= url;
				final Bitmap curBitmap = BitmapFactory.decodeStream(url.openStream());
				itemView.setIcon(curBitmap); //아이콘은 비트맵으로 세팅해준다.
			}

		} 
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return itemView;
	}
	
}
