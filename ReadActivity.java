package com.example.diarybaby;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 일기보기 선택을 위한 클래스
 * @author 박성호
 *
 */
public class ReadActivity extends Activity            
{
	/**
	 * 액티비티 사용을 위한 메소드
 	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		final Intent wintent = new Intent(this,DiaryActivity.class);
		setContentView(R.layout.activity_read);
		Gallery gal = (Gallery) findViewById(R.id.gallery);    //갤러리를 사용     
		gal.setAdapter(new ImageAdapter(this));                //갤러리에 이미지를 공급 받기 위한 어뎁터     
		gal.setOnItemSelectedListener(new OnItemSelectedListener() { 
			public void onItemSelected(AdapterView<?> parent, View view,int position, long id) //이미지가 선택됬을때 이벤트 처리 메소드
			{
				
				Toast.makeText(ReadActivity.this, position+1 + "번째 일기",
						Toast.LENGTH_SHORT).show();
				DiaryActivity.pos=position;
				startActivity(wintent);  
			}	
			public void onNothingSelected(AdapterView<?> parent)  //그림이 아무것도 선택되지 않았을때 처리
			{} 
		});
	}
}

/**
 * 이미지 공급을 위한 어뎁터 클래스
 * @author 박성호
 *
 */
class ImageAdapter extends BaseAdapter  
{
	private Context mContext;
	private SQLiteDatabase db;
	private ArrayList<String> images=new ArrayList<String>();
	private Cursor cursor;
				
	/**
	 * 생성자
	 * @param Context c
	 */
	public ImageAdapter(Context c)   
	{
		mContext = c; 
		db=MainActivity.dbHelper.getReadableDatabase();
		cursor=db.rawQuery("SELECT image  FROM diary",null);
		while(cursor.moveToNext())
		{
			images.add(cursor.getString(cursor.getColumnIndex("image"))); //데이터베이스의 모든 이미지이름을 어레이 리스트에 넣는다.
		}
		cursor.close(); 
		MainActivity.dbHelper.close();
	}
	/**
	 * 크기를 얻는 메소드
	 * @return int
	 */
	public int getCount()               
	{
		return images.size();
	}
	/**
	 * 위치로 오브젝트를 얻는 메소드
	 * @param int position
	 * @return Object
	 */
	public Object getItem(int position)  
	{
		
		return images.get(position);
	}
	/**
	 * 위치로 아이디를 얻는 메소드
	 * @param int position
	 * @return long
	 */
	public long getItemId(int position)
	{
		return position;              
	}
	/**
	 * 이미지를 얻는 메소드 
	 * @param int position
	 * @param View convertView
	 * @param ViewGroup parent
	 * @return View
	 */
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		ImageView imageView;              
		if (convertView == null) 
		{
			imageView = new ImageView(mContext);  
		} 
		else 
		{
			imageView =(ImageView)convertView; 
		}
		       
		imageView.setImageURI(Uri.parse(images.get(position)));  //URI 파싱을 통해 이미지 이름을 통해 이미지를 띠운다.
		imageView.setScaleType(ImageView.ScaleType.FIT_XY);         
		imageView.setLayoutParams(new Gallery.LayoutParams(300, 200)); 
		return imageView; 
	}
}