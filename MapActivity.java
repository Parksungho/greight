package com.example.diarybaby;

import java.io.IOException;
import java.util.*;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;
/**
 * 구글맵을 위한 클래스
 * @author 박성호
 *
 */
public class MapActivity extends FragmentActivity implements LocationListener,OnMarkerClickListener 
{
	private GoogleMap mmap;
	private LocationManager locationManager;
	private String provider;
	private String text;                  //구글맵과 프로바이더 선언
	/**
	 * 액티비티 사용을 위한 메소드
	 * @param Bundle savedInstanceState
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		GooglePlayServicesUtil.isGooglePlayServicesAvailable(MapActivity.this);        
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);    //구글맵 서비스를 받기 위한 준비
		Criteria criteria = new Criteria();     //가장 알맞은 프로바이더를 받기위한 선언
		provider = locationManager.getBestProvider(criteria, true);  //상황에 맞는 프로바이더를 추출해냄
		Toast.makeText(this, provider, Toast.LENGTH_SHORT).show();
		locationManager.requestLocationUpdates(provider, 1, 1, MapActivity.this);  //프로바이더로 부터 마지막 위치를 얻어온다.
		setUpMapIfNeeded(); 
	}
	/**
	 * 뒤로가기 버튼이 눌렸을때 처리
	 */
	@Override
	public void onBackPressed() 
	{
		this.finish();               
	}
	/**
	 * 다시 포커스를 얻어왔을때 처리
	 */
	@Override
	protected void onResume() 
	{
		super.onResume();
		locationManager.requestLocationUpdates(provider, 1, 1, MapActivity.this);
		setUpMapIfNeeded();    
	}
	/**
	 * 포커스를 잃었을때 처리
	 */
	@Override
	protected void onPause() 
	{
		super.onPause();
		locationManager.removeUpdates(this);     
	}
	/**
	 * 맵보여주는 화면을 가공하는 메소드
	 */
	private void setUpMapIfNeeded()
	{
		Location location = locationManager.getLastKnownLocation(provider);  //프로바이더로 부터 위치를 얻어오고
		LatLng position = new LatLng(location.getLatitude(),location.getLongitude());  //위도경도 위치를 얻어온다.
		if (mmap == null) 
		{   
			mmap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15)); 
			mmap.addMarker(new MarkerOptions().position(position));    //맵을 보여주는 위치와 줌 정도, 마커를 표시
		}
		if (mmap != null) 
		{
			setUpMap();
			mmap.addMarker(new MarkerOptions().position(position));  
		}
		
	}
	/**
	 * 마커클릭리스너 및 옵션 설정
	 */
	private void setUpMap()
	{
		mmap.setOnMarkerClickListener(this);
		mmap.setMyLocationEnabled(true);
		mmap.getMyLocation();                
	}
	boolean locationTag = true;
	/**
	 * 위치가 바뀌었을때 호출 되는 메소드
	 * @param Location location
	 */
	@Override
	public void onLocationChanged(Location location) 
	{
		if (locationTag) 
		{
			Log.d("myLog", "onLocationChanged: !!" + "onLocationChanged!!");
			double lat = location.getLatitude();
			double lng = location.getLongitude();
			Toast.makeText(MapActivity.this, "위도  : " + lat + " 경도: " + lng,Toast.LENGTH_SHORT).show();
			locationTag = false;
		}
	}
	/**
	 * 프로바이더가 비활성화 될때 메소드
	 * @param String provider
	 */
	@Override
	public void onProviderDisabled(String provider)
	{
		
	}
	/**
	 * 프로바이더가 활성화 될때 메소드
	 * @param String provider
	 */
	@Override
	public void onProviderEnabled(String provider)
	{
		
	}
	/**
	 * 프로바이더가 바뀔때 처리 메소드
	 * @param String provider
	 * @param int status
	 * @param Bundle extras
	 */
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras)
	{
		
	}
	/**
	 * 마커 클릭 이벤트 처리 메소드 
	 * @param Marker marker
	 * @return boolean
	 */
	@Override
	public boolean onMarkerClick(Marker marker)   //해당위치의 마커를 클릭하면 주소를 보여주고 쓰기화면에 주소를 넘겨준다.
	{
		String str;
		
		marker.getPosition(); 
		marker.showInfoWindow();
		marker.setTitle("현재위치"); 
		marker.setSnippet(getAddres(marker.getPosition().latitude,marker.getPosition().longitude)); 
		str = getAddres(marker.getPosition().latitude,marker.getPosition().longitude);
		Intent ointent = new Intent();
		ointent.putExtra("TextOut", str);
		setResult(RESULT_OK, ointent);    
		return true;
	}
	/**
	 * 위도경도를 한글주소로 변환하는 메소드
	 * @param double lat
	 * @param double lng
	 * @return String
	 */
	public String getAddres(double lat, double lng)  
	{
		int lt=(int)lat,lg=(int)lng;
		Geocoder gcK = new Geocoder(this,Locale.KOREA);	//Geocoder 를 이용해 한글 주소를 변환한다.
		String res = "null";
		try {
			List<Address> addresses = gcK.getFromLocation(lat, lng, 5); 
			
			StringBuilder sb = new StringBuilder();
			if(addresses.size()>0)
			{
				Address address = addresses.get(0);
				sb.append(address.getCountryName()).append(" "); 
				sb.append(address.getLocality()).append(" ");      
				sb.append(address.getThoroughfare()).append(" ");  
				sb.append(address.getFeatureName()).append(" ");    
				res = sb.toString(); 
			}
			else
			{
				res="한글 주소 변환 실패 위도:"+lt+"경도:"+lg;
			}
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return res;
	}
}
