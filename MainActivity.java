package com.example.diarybaby;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

/**
 * 메인화면을 위한 클래스
 * @author 박성호
 *
 */
public class MainActivity extends Activity  
{ 
	
	static DatabaseActivity dbHelper;    //데이터베이스 도우미 클래스
	/**
	 * 액티비티 사용을 위한 메소드
	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Intent intent = new Intent(this, FaceActivity.class);
		startActivity(intent);        //로고 페이지를 먼저 띄우고          
		setContentView(R.layout.activity_main);  //메인화면을 띄운다 
		dbHelper=new DatabaseActivity(this);  
	}
	/**
	 * 버튼이 눌렸을때 이벤트 처리 메소드
	 * @param View v
	 */
	public void mOnClick(View v) 
	{
		switch (v.getId()) {
		case R.id.btn1: //일기쓰기 화면으로 넘어간다
			Intent wintent = new Intent(this, WriteActivity.class);
			startActivity(wintent);
			break;
			
		case R.id.btn2: //일기보기 화면으로 넘어간다.
			Intent rintent = new Intent(this, ReadActivity.class);
			startActivity(rintent); 
			break;
		case R.id.btn3: //종료 여부를 묻는 대화상자를 보여준다.
			new AlertDialog.Builder(this).setTitle(" ")
					.setMessage("종료하시겠습니까?")
					.setPositiveButton("YES", new DialogInterface.OnClickListener(){
						public void onClick(DialogInterface dialog, int which)
						{
							finish();
						}
					})
					.setNegativeButton("NO", null).setIcon(R.drawable.bye)
					.show();
			
			break;
		}
	}
	/**
	 * 메뉴를 위한 메소드
	 * @param Menu menu
	 * @retuen boolean
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
