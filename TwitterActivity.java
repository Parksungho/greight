package com.example.diarybaby;

import java.util.Date;
import java.util.List;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 트위터 작업을 위한 클래스
 * @author 박성호
 *
 */
public class TwitterActivity extends Activity 
{
	public static final String TAG = "TwitterActivity";
	TextView nameText;
	Button connectBtn;
	StatusListView statusList;
	StatusListAdapter statusAdapter; //트위터에서 타임라인을 받기위한 Status 객체 처리를 위한 선언
	Button writeBtn;
	EditText writeInput;
	
	Handler mHandler = new Handler();
	/**
	 * 액티비티 사용을 위한 메소드
	 * @param Bundle savedInstanceState
	 */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitter);

        connectBtn = (Button) findViewById(R.id.connectBtn);
        connectBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v)  //연결하기 버튼이 눌리면 함수 호출
        	{
        		connect();
        	}
        });

        nameText = (TextView) findViewById(R.id.nameText);

        statusList = (StatusListView) findViewById(R.id.statusList);
        statusAdapter = new StatusListAdapter(this, mHandler);
        statusList.setAdapter(statusAdapter);
        statusList.setOnDataSelectionListener(new OnDataSelectionListener() {
			public void onDataSelected(AdapterView parent, View v, int position, long id)  //타임라인글을 선택하면 내용을 토스트로 띄어준다.
			{
				Status curItem = (Status) statusAdapter.getItem(position);
				String curText = curItem.getText();
				Toast.makeText(getApplicationContext(), "Selected : " + curText, 1000).show();
			}
		});

        writeBtn = (Button) findViewById(R.id.writeBtn);
        writeBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) 
        	{
        		String statusText = writeInput.getText().toString();
        		if (statusText.length() < 1) //만약 글이 입력이 0글자이면
        		{
        			Toast.makeText(getApplicationContext(), "글을 입력 해주세요.", 1000).show();
        			return;
        		}
        		updateStatus(statusText); //글쓰기 버튼이 눌리고 상태 업데이트 함수를 호출
        	}
        });

        writeInput = (EditText) findViewById(R.id.writeInput);
    }
    /**
     * 타임라인 업데이트를 위한 메소드
     * @param String statusText1
     */
    private void updateStatus(String statusText1) 
    {
    	String statusText=statusText1;
    	try
    	{
    		Status status = TwitInfo.TwitInstance.updateStatus(statusText); //status에 텍스트 업데이트될 객체로 저장
    		final Date curDate = status.getCreatedAt();   //날짜를 받아온다
    		Toast.makeText(getApplicationContext(), "글쓰기 완료 : " + TwitInfo.DateFormat.format(curDate), Toast.LENGTH_SHORT).show();
    		showUserTimeline(); //업데이트후 타임라인을 보여주는 메소드 호출
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    }
    /**
     * 트위터 인증을 위한 메소드
     */
    private void connect() 
    {
		Log.d(TAG, "connect() called.");

		if (TwitInfo.TwitLogin) //트위터가 인증되어 있을때 처리
		{
			Log.d(TAG, "twitter already logged in.");
			Toast.makeText(getBaseContext(), "twitter already logged in.", Toast.LENGTH_LONG).show();
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();

				builder.setOAuthAccessToken(TwitInfo.TWIT_KEY_TOKEN);
				builder.setOAuthAccessTokenSecret(TwitInfo.TWIT_KEY_TOKEN_SECRET);
				builder.setOAuthConsumerKey(TwitInfo.TWIT_CONSUMER_KEY);
				builder.setOAuthConsumerSecret(TwitInfo.TWIT_CONSUMER_SECRET);  //빌더에 트위터인포 클래스의 정보를 넣어준다.

				Configuration config = builder.build();
				TwitterFactory tFactory = new TwitterFactory(config); 
				TwitInfo.TwitInstance = tFactory.getInstance();  //트위터 인포의 트위터 객체 속성에 빌더로 만든 정보 입력

				Toast.makeText(getBaseContext(), "twitter connected.", Toast.LENGTH_LONG).show();

	    	} 
			catch (Exception ex) 
			{
				ex.printStackTrace();
			}
			showUserTimeline(); //인증완료후 타임라인 글을 보여준다.
		} 
		else  //인증이 안되어 있을때 처리
		{
			try
			{
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setDebugEnabled(true);
				builder.setOAuthConsumerKey(TwitInfo.TWIT_CONSUMER_KEY);
				builder.setOAuthConsumerSecret(TwitInfo.TWIT_CONSUMER_SECRET);

				TwitterFactory factory = new TwitterFactory(builder.build());
				Twitter mTwit = factory.getInstance();
				final RequestToken mRequestToken = mTwit.getOAuthRequestToken();
				String outToken = mRequestToken.getToken();
				String outTokenSecret = mRequestToken.getTokenSecret();

				Log.d(TAG, "Request Token : " + outToken + ", " + outTokenSecret);
				Log.d(TAG, "AuthorizationURL : " + mRequestToken.getAuthorizationURL());

				TwitInfo.TwitInstance = mTwit;
				TwitInfo.TwitRequestToken = mRequestToken;
				
				Intent intent = new Intent(getApplicationContext(), TwitLog.class);
				intent.putExtra("authUrl", mRequestToken.getAuthorizationURL());
				startActivityForResult(intent, TwitInfo.REQ_CODE_TWIT_LOGIN);
			}
	    	 catch (Exception ex) 
	    	 {
				ex.printStackTrace();
			}
    	}
    }
    
    /**
     * 인텐트 메세지 처리를 위한 메소드
     * @param int requestCode
     * @param int resultCode
     * @param Intent resultIntent
     */
	protected void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
		super.onActivityResult(requestCode, resultCode, resultIntent);

		if (resultCode == RESULT_OK) {
			if (requestCode == TwitInfo.REQ_CODE_TWIT_LOGIN) 
			{
				
				Intent rIntent=resultIntent;
				try //인증이 안되었을때 유효한 토큰을 처리하기 위한 처리
				{
					Twitter mTwit = TwitInfo.TwitInstance;
					AccessToken mAccessToken = mTwit.getOAuthAccessToken(TwitInfo.TwitRequestToken, resultIntent.getStringExtra("oauthVerifier"));
					TwitInfo.TwitLogin = true;
					TwitInfo.TWIT_KEY_TOKEN = mAccessToken.getToken();
					TwitInfo.TWIT_KEY_TOKEN_SECRET = mAccessToken.getTokenSecret();
					TwitInfo.TwitAccessToken = mAccessToken;
					TwitInfo.TwitScreenName = mTwit.getScreenName();
					Toast.makeText(getBaseContext(), "Twitter connection succeeded : " + TwitInfo.TWIT_KEY_TOKEN, Toast.LENGTH_LONG).show();
					showUserTimeline();
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		}
	}
	
	private void showUserTimeline() {
		Log.d(TAG, "showUserTimeline() called.");

		connectBtn.setVisibility(View.GONE);
        nameText.setVisibility(View.VISIBLE);
        nameText.setText(TwitInfo.TwitScreenName);
        Twitter mTwit = TwitInfo.TwitInstance;
        try {
			final List<Status> statuses = mTwit.getUserTimeline();

			mHandler.post(new Runnable() {
				public void run() {
					statusAdapter.setListItems(statuses);
					statusAdapter.notifyDataSetChanged();
				}
			});
			
		} 
        catch(Exception ex) 
        {
			ex.printStackTrace();
		}
	}
	/**
	 * 포커스를 잃었을때 메소드
	 */
	protected void onPause() 
	{
		super.onPause();
		saveProperties();  //정보를 저장한다
	}
	/**
	 * 포커스를 다시 얻었을때
	 */
	protected void onResume()
	{
		super.onResume();
		loadProperties(); //정보를 얻오온다.
	}
	/**
	 * 속성을 저장하는 메소드
	 */
	private void saveProperties() 
	{
		SharedPreferences pref = getSharedPreferences("TWIT", MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean("TwitLogin", TwitInfo.TwitLogin);
		editor.putString("TWIT_KEY_TOKEN", TwitInfo.TWIT_KEY_TOKEN);
		editor.putString("TWIT_KEY_TOKEN_SECRET", TwitInfo.TWIT_KEY_TOKEN_SECRET);
		editor.commit();
	}
	/**
	 * 속성을 로드하는 메소드
	 */
	private void loadProperties() 
	{
		SharedPreferences pref = getSharedPreferences("TWIT", MODE_PRIVATE);
		TwitInfo.TwitLogin = pref.getBoolean("TwitLogin", false);
		TwitInfo.TWIT_KEY_TOKEN = pref.getString("TWIT_KEY_TOKEN", "");
		TwitInfo.TWIT_KEY_TOKEN_SECRET = pref.getString("TWIT_KEY_TOKEN_SECRET", "");
	}
}

interface OnDataSelectionListener  //타임라인을 선택했을때 처리를 위한 처리 인터페이스
{
	public void onDataSelected(AdapterView parent, View v, int position, long id);
}