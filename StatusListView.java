package com.example.diarybaby;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
/**
 * 타임라인을 받아오기 위한 클래스
 * @author 박성호
 *
 */
public class StatusListView extends ListView
{
	private StatusListAdapter adapter;
	private OnDataSelectionListener selectionListener;
	/**
	 * 생성자
	 * @param Context context
	 */
	public StatusListView(Context context) 
	{
		super(context);
		init();
	}
	/**
	 * 생성자 오버로딩
	 * @param Context context
	 * @param AttributeSet attrs
	 */
	public StatusListView(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		init();
	}
	/**
	 * 속성들 초기화 메소드
	 */
	private void init() 
	{
        setOnItemClickListener(new OnItemClickAdapter());
	}

	/**
	 * 아뎁터를 통한 타임라인 한줄당 객체로 전달받기 위한 어뎁터 설정 메소드
	 * @param BaseAdapter adapter
	 */
	public void setAdapter(BaseAdapter adapter) 
	{
		super.setAdapter(adapter);

	}
	/**
	 * 어뎁더로 데이터를 얻는 메소드
	 * @return BaseAdapter
	 */
	public BaseAdapter getAdapter()
	{
		return (BaseAdapter)super.getAdapter();
	}

	/**
	 * 리스너 등록 메소드
	 * @param OnDataSelectionListener listener
	 */
	public void setOnDataSelectionListener(OnDataSelectionListener listener)
	{
		this.selectionListener = listener;
	}

	/**
	 * 리스너 얻는 메소드
	 * @return  OnDataSelectionListener
	 */
	public OnDataSelectionListener getOnDataSelectionListener()
	{
		return selectionListener;
	}
	/**
	 * 데이터를 제공하는 어뎁터 클래스
	 * @author 박성호
	 *
	 */
	class OnItemClickAdapter implements OnItemClickListener
	{
		/**
		 * 생성자
		 */
		public OnItemClickAdapter() 
		{
		}
		/**
		 * 리스너를 통해 데이터가 클릭 되었을때 처리를 위한 메소드
		 * @param AdapterView parent
		 * @param  View v
		 * @param  int position
		 * @param long id
		 */
		public void onItemClick(AdapterView parent, View v, int position, long id) 
		{
			if (selectionListener == null)
			{
				return;
			}
			selectionListener.onDataSelected(parent, v, position, id);
		}
	}
}