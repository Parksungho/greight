package com.example.diarybaby;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
/**
 * 일기보기를 위한 클래스
 * @author 박성호
 *
 */
public class DiaryActivity extends Activity  
{ 
	private SQLiteDatabase db;
	private Cursor cursor;
	private ImageView img;
	private Button del;
	private TextView date,map,rec;
	 static int pos;             //데이터베이스에 저장된 정보를 읽기위한 데이터베이스와 각 요소들 선언
	/**
	 * 액티비티 사용을 위한 메소드
	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_readdiary); 
		img=(ImageView)findViewById(R.id.readImageview);
		map=(TextView)findViewById(R.id.readTextView1);
		del=(Button)findViewById(R.id.readButton2);
		date=(TextView)findViewById(R.id.readTextview);
		rec=(TextView)findViewById(R.id.readTextview2);  //요소들을 아이디로 채워준다.
		db=MainActivity.dbHelper.getReadableDatabase();		//매인의 데이터베이스를 읽기 전용으로 연다.
		cursor=db.rawQuery("SELECT date,position,record,image  FROM diary",null); 
		if(pos==0) //ReadActivity 로 부터 전달받은 포지션 정보
		{
			cursor.moveToFirst();
			img.setImageURI(Uri.parse(cursor.getString(cursor.getColumnIndex("image"))));
			map.setText(cursor.getString(cursor.getColumnIndex("position")));
			date.setText(cursor.getString(cursor.getColumnIndex("date")));
			rec.setText(cursor.getString(cursor.getColumnIndex("record")));            //만약 첫번째 그림이 선택된다면 데이터베이스에서 첫번째 열에 정보로 채운다.
		}
		else  //첫번째 그림이 아니라면
		{
			if(!cursor.moveToNext())
				cursor.moveToLast();     //커서가 마지막이라면 마지막으로 이동하고
			
			else
			{	
				for(int i=0;i<pos;i++)
					cursor.moveToNext();   //커서가 마지막이 아니라면 전달받은 pos만큼 이동하여 데이타베이스 정보로 채운다.
				
				img.setImageURI(Uri.parse(cursor.getString(cursor.getColumnIndex("image"))));
				map.setText(cursor.getString(cursor.getColumnIndex("position")));
				date.setText(cursor.getString(cursor.getColumnIndex("date")));
				rec.setText(cursor.getString(cursor.getColumnIndex("record")));
			}
		}
		cursor.close();
		MainActivity.dbHelper.close();  //사용한 커서와 데이터베이스 반납
	}
	/**
	 * 지우기 버큰에 대한 이벤트처리 메소드
	 * @param View v
	 */
	public void mOnClick(View v) //지우기 버튼이 눌려졌을때 이벤트 처리
	{
		db=MainActivity.dbHelper.getReadableDatabase();
		cursor=db.rawQuery("SELECT date,position,record,image  FROM diary",null);
		switch (v.getId()) {
		case R.id.readButton2:
			if(!cursor.moveToNext())
			{
				cursor.moveToLast(); //커서가 마지막이면 마지막으로 이동하고
			}
			else
			{	
				for(int i=0;i<pos;i++)
				{
					cursor.moveToNext();  //아니면 pos만큼 이동한 후에 
				}
				db=MainActivity.dbHelper.getReadableDatabase();
				String str=cursor.getString(cursor.getColumnIndex("image"));  //이미지 칼럼의 인덱스를 얻어와서
				db.execSQL("DELETE FROM diary WHERE image='"+str+"'");    //지우려는 그림의 이미지 이름의 행을 지운다.
				MainActivity.dbHelper.close();
			}
			cursor.close();
			MainActivity.dbHelper.close();
			finish();
		}
	}
	
}
