package com.example.diarybaby;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
/**
 * 트위터 Status를 위한 커스텀뷰
 * @author 박성호
 *
 */
public class StatusItemView extends LinearLayout 
{

	private ImageView mIcon;
	private TextView mText01;
	private TextView mText02;
	private TextView mText03;
	/**
	 * 커스텀뷰 생성자
	 * @param Context context
	 */
	public StatusItemView(Context context)
	{
		super(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.activity_list, this, true);  //인플레이트 서비스를 얻어와서 직접 인플레이트 시킨다.
		mIcon = (ImageView) findViewById(R.id.iconItem);
		mText01 = (TextView) findViewById(R.id.dataItem01);
		mText02 = (TextView) findViewById(R.id.dataItem02);
		mText03 = (TextView) findViewById(R.id.dataItem03); //각 요소들을 객체화 시켜준다.
	}

	/**
	 * 텍스트를 설정
	 * @param int index
	 * @param String data
	 */
	public void setText(int index, String data) //각 텍스트뷰에 텍스트를 채워주는 메소드
	{
		if (index == 0) {
			mText01.setText(data);
		} else if (index == 1) {
			mText02.setText(data);
		} else if (index == 2) {
			mText03.setText(data);
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 *	아이콘 설정
	 * @param Drawable icon
	 */
	public void setIcon(Drawable icon)  //아이콘을 채워주는 메소드
	{
		mIcon.setImageDrawable(icon);
	}
	/**
	 *	비트맵을 위한 아이콘 설정
	 * @param Bitmap bitmap
	 */
	public void setIcon(Bitmap bitmap)  //아이콘을 채워주는 메소드
	{
		mIcon.setImageBitmap(bitmap);
	}
}
