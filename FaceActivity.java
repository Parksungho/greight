package com.example.diarybaby;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;

/**
 * 로딩 화면을 위한 클래스
 * @author 박성호
 *
 */
public class FaceActivity extends Activity 
{
	private CountDownTimer timer = null;
	private Vibrator mVib;                    //카운트 다운타이머를 이용한 3초간 로딩화면  

	/**
	 * 액티비티 사용을 위한 메소드
	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		mVib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE); 
		timer = new CountDownTimer(3000, 1000) {     //3초동안 타이머 실행
			public void onTick(long millisUntilFinished)  //3초동안 1초씩 반복해서 호출되는 메소드
			{
				setContentView(R.layout.activity_face);
				mVib.vibrate(500); 
			}
			public void onFinish()  //3초가 지나고 발생하는 메소드
			{
				finish(); 
			}
		}.start(); 
	}
}