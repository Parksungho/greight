package com.example.diarybaby;
import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.*;
import android.provider.MediaStore;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 일기 쓰기화면을 위한 메소드
 * @author 박성호
 *
 */
public class WriteActivity extends Activity      
{
	
	final static int CAM_EDIT = 0;
	final static int GAL_EDIT = 1;
	final static int WRI_EDIT = 2;
	final static int WRI2_EDIT = 3;      //인텐트 메시지를 위한 선언
	private EditText text;
	private ImageView img;		
	private TextView addT;
	private TextView timeT;
	private int mYear,mMonth,mDay;
	private String galU;
	/**
	 * 액티비티 사용을 위한 메소드
	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_write);
		addT = (TextView)findViewById(R.id.bt);
		text = (EditText) findViewById(R.id.editText1);
		img = (ImageView) findViewById(R.id.imageView1); 
		timeT=(TextView)findViewById(R.id.textView1);
		Calendar cal=new GregorianCalendar();      //날짜 대화상자를 위한 달력
		mYear=cal.get(Calendar.YEAR);
		mMonth=cal.get(Calendar.MONTH);
		mDay=cal.get(Calendar.DAY_OF_MONTH);
	}
	/**
	 * 버튼 클릭을 처리 하기 위한 메소드
	 * @param View v
	 */
	public void mOnClick(View v) 
	{
		SQLiteDatabase db;
		ContentValues row;
		switch (v.getId()) 
		{
		case R.id.button1:  //버큰 1이 눌리면 카메라나 갤러리를 선택하는 대화상자 띠운다.
			new AlertDialog.Builder(this).setTitle(" ").setMessage("선택해 주세요!!")
					.setPositiveButton("카메라", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which)       
						{
							
							Intent cIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);  //카메라를 사용할 수 있게 해준다.
							File file = new File(Environment.getExternalStorageDirectory(),"image.jpg");  
							cIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));  
							startActivityForResult(cIntent, CAM_EDIT); 
						}
					})
					.setNegativeButton("갤러리",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which)   
						{
							
							Intent intent = new Intent(Intent.ACTION_PICK);     //갤러리를 사용할 수 있게 해준다.
							intent.setType("image/*");                         
							startActivityForResult(intent,GAL_EDIT);    
						}
					})
					.setIcon(R.drawable.camedilog).show(); 
			break;
			
		case R.id.button2:
			//버튼 2가 눌리면 맵화면으로 전환
			MapThread MThread=(MapThread)new MapThread().execute((Void)null);
			break;
			
		case R.id.button3: //버튼 3이 눌리면 트위터 화면으로 전환
			TwitThread TThread=(TwitThread)new TwitThread().execute((Void)null);
			break;
		case R.id.button4: //버튼4가 눌리면 데이터베이스에 쓴 정보들이 저장된다.
			String r1,r2,r3,r4;
			db=MainActivity.dbHelper.getWritableDatabase();
			row=new ContentValues();
			r1=timeT.getText().toString();
			r2=addT.getText().toString();
			r3=text.getText().toString();
			r4=galU;
			row.put("date", r1);
			row.put("position", r2);
			row.put("record", r3);
			row.put("image", r4);
			db.insert("diary", null, row);
			MainActivity.dbHelper.close();
			Toast.makeText(WriteActivity.this,"저장되었습니다.",Toast.LENGTH_SHORT).show();
			break; 
		case R.id.button5: //버튼5가 눌리면 날짜선택 대화상자가 띠어준다.
			new DatePickerDialog(WriteActivity.this,mListener,mYear,mMonth,mDay).show();
			break;
		}
	}
	DatePickerDialog.OnDateSetListener mListener=new DatePickerDialog.OnDateSetListener() //대화상자 선택을 위한 리스너
	{
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) 
		{
			mYear=year;
			mMonth=monthOfYear;
			mDay=dayOfMonth;
			timeT.setText(String.format("%d/%d/%d",mYear,mMonth+1,mDay));
		}
	};
	/**
	 * 트위터 작업을 위한 멀티쓰레드 클래스
	 * @author 박성호
	 *
	 */
	class TwitThread extends AsyncTask<Void,Void,Void>
	{

		@Override
		/**
		 * @param Void... arg0
		 * @return Void
		 */
		protected Void doInBackground(Void... arg0)
		{
			connect();
			return null;
		}
		@Override
		/**
		 * @param Void... values
		 */
		protected void onProgressUpdate(Void... values)
		{
			connect();
			super.onProgressUpdate(values);
		}
		/**
		 * 트위터 화면으로 넘어가는 메소드
		 */
		private void connect() 
		{
			Intent tin=new Intent(WriteActivity.this,TwitterActivity.class);
			startActivity(tin);
		}
	}
	/**
	 * 인텐트 메세지 처리를 위한 메소드
	 * @param int requestCode
	 * @param int resultCode
	 * @param Intent data
	 */
	protected void onActivityResult(int requestCode, int resultCode,Intent data)  
	{
		super.onActivityResult(requestCode, resultCode, data);
		if (data == null)
			return;
		if(requestCode==CAM_EDIT)  //카메라로 찍고 이미지를 얻어온다.
		{
			img.setImageURI(getLastCaptureImageUri());
			galU=getLastCaptureImageUri().toString();
		} 
		else if(requestCode==GAL_EDIT)  //갤러리로 선택된 이미지를 얻어온다.
		{
			img.setImageURI(data.getData());         	
			galU=data.getDataString();
			
		}
		else if (requestCode == WRI_EDIT)  //맵에서 마커가 클리되면 주소를 얻어온다.
		{
			if (resultCode == RESULT_OK)					
			{
				addT.setText(data.getStringExtra("TextOut"));
			}
		}
		else if (requestCode == TwitInfo.REQ_CODE_TWIT_LOGIN) //트위터 로그인이 되면 토스트를 출력해준다.
		{
			if (resultCode == RESULT_OK) 
			{
				Toast.makeText(getBaseContext(), "twitter connected.", Toast.LENGTH_LONG).show();
			}
		}
	}
	/**
	 * 마지막 Uri를 추출하기 위한 메소드
	 * @return Uri
	 */
	private Uri getLastCaptureImageUri()
	{
		Uri uri=null;
		String[] IMAGE_PROJECTION={MediaStore.Images.ImageColumns.DATA,MediaStore.Images.ImageColumns._ID};
		try
		{
			Cursor cursorImages=getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
					IMAGE_PROJECTION, null, null, null);
			if(cursorImages!=null&&cursorImages.moveToLast())
			{
				uri=Uri.parse(cursorImages.getString(0));
				int id = cursorImages.getInt(1);
				cursorImages.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return uri;
	}	
	/**
	 * 맵화면을 위한 멀티 쓰레드 클래스
	 * @author 박성호
	 *
	 */
	class MapThread extends AsyncTask<Void,Void,Void>
	{
		/**
		 * @param Void... arg0
		 * @return Void
		 */
		@Override
		protected Void doInBackground(Void... arg0)
		{
			Intent mintent = new Intent(WriteActivity.this, MapActivity.class);
			startActivityForResult(mintent, WRI_EDIT);  
			return null;
		}
		/**
		 * @param Void... values
		 */
		@Override
		protected void onProgressUpdate(Void... values)
		{
			Intent mintent = new Intent(WriteActivity.this, MapActivity.class);
			startActivityForResult(mintent, WRI_EDIT);  
			super.onProgressUpdate(values);
		}
	}
	
}
