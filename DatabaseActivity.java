package com.example.diarybaby;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
/**
 * 데이터베이스를 위한 도우미 클래스
 * @author 박성호
 *
 */
class DatabaseActivity extends SQLiteOpenHelper
{
	/**
	 * DatabaseActivity 생성자
	 * @param Context context
	 */
	public DatabaseActivity(Context context)
	{
		super(context,"wherego.db",null,1);
	}
	/**
	 * 데이터 베이스가 생성 될때 호출 되는 메소드
	 * @param SQLiteDatabase db
	 */
	public void onCreate(SQLiteDatabase db) //도우미 클레스를 굳이 호출 하지 않아도 읽기전용, 쓰기 전용으로 데이터베이스 접근해도 호출 되는 메소드
	{
		db.execSQL("CREATE TABLE " + "diary" + " (" +"_id" + " integer primary key autoincrement, "
				+"date" + " text not null, "
				+ "position" + " text not null, "
				+ "record" + " text not null, "
				+ "image" + " text not null )");
	}
	/**
	 * 데이터베이스가 업데이트 될때 호출 되는 메소드
	 * @param SQLiteDatabase db
	 * @param int oldVersion
	 * @param int newVersion
	 */
	public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion) //데이터베이스 테이블을 다시 새롭게 만드는 메소드
	{
		db.execSQL("DROP TABLE IF EXISTS diary");
		onCreate(db);
	}
}